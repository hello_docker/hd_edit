//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app.js');
let should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe('Test hello Docker', () => {
 /*
  * Test the /GET route
  */
  describe('/GET true false', () => {
      it('it should return 200 status code', (done) => {
        chai.request(server)
            .get('/')
            .end((err, res) => {
              res.should.have.status(200);
              done();
            });
      });
            
      it('it should return json with true and false', (done) => {
        chai.request(server)
            .get('/test/truefalse')
            .end((err, res) => {
                res.body.testTrue.should.be.true;
                res.body.testFalse.should.be.false;
              done();
            });
      });
  });

});